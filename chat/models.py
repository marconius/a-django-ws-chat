from dataclasses import dataclass
from typing import Dict, FrozenSet

from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse

from .exceptions import InvalidChatRoomName


class Message(models.Model):
    message = models.TextField()
    room_name = models.CharField(max_length=255, db_index=True)
    time_received = models.DateTimeField(default=False)
    user = models.ForeignKey(User, models.CASCADE)

    def __str__(self):
        return '{user} in {room_name}: {message} [{time_received}]'.format(
            **self.as_dict())

    def as_dict(self) -> Dict[str, str]:
        return dict(
            user=self.user.username,
            message=self.message,
            room_name=self.room_name,
            time_received=self.time_received.isoformat())


@dataclass
class ChatRoom:
    """Not a real model, but could easily become one if `Room` becomes more rich.
    We will leave as is to avoid having to wrap in sync_to_async in our async code.
    """
    member_usernames: FrozenSet[str]
    user_name_delimeter = '_'

    def get_room_name(self) -> str:
        return self.user_name_delimeter.join(sorted(self.member_usernames))

    @classmethod
    def from_room_name(cls, room_name):
        usernames = room_name.split(cls.user_name_delimeter)
        if usernames != sorted(usernames):
            raise InvalidChatRoomName('Invalid room name')
        members: FrozenSet[str] = frozenset(room_name.split(cls.user_name_delimeter))
        return cls(member_usernames=members)

    def check_is_member(self, username: str) -> bool:
        return username in self.member_usernames

    def get_absolute_url(self):
        return reverse('chat-room', args=[self.get_room_name()])
