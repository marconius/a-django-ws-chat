import datetime

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncJsonWebsocketConsumer

from .models import ChatRoom, Message


class ChatConsumer(AsyncJsonWebsocketConsumer):

    async def connect(self):

        self.room_group_name = None
        self.room_name = self.scope['url_route']['kwargs']['room_name']

        if not (self._is_authenticated() and self._is_authorized()):
            return await self.close()

        # Join room group
        self.room_group_name = 'chat_%s' % self.room_name
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

        self.archived_messages = await self.get_archived_messages()
        await self.send_json(content={
            'messages': self.archived_messages,
        })


    async def disconnect(self, close_code):
        """ Leave room group if necessary """

        if self.room_group_name:
            await self.channel_layer.group_discard(
                self.room_group_name,
                self.channel_name
            )

    async def receive_json(self, content):
        """ Receive message from WebSocket"""

        now = datetime.datetime.now()
        message = Message(
            room_name=self.room_name,
            message=content['message'],
            time_received=now,
            user=self.scope['user'],
        )
        await persist_message(message)

        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message.as_dict()
            }
        )

    async def chat_message(self, event):
        """ Receive message from room group """

        # Send message to WebSocket
        await self.send_json(content={
            'messages': [event['message']]
        })


    @database_sync_to_async
    def get_archived_messages(self):
        return [msg.as_dict() for msg in Message.objects.filter(room_name=self.room_name)]

    def _is_authenticated(self) -> bool:
        return self.scope['user'].is_authenticated

    def _is_authorized(self) -> bool:
        room = ChatRoom.from_room_name(self.room_name)
        return room.check_is_member(self.scope['user'].username)

@database_sync_to_async
def persist_message(message: Message) -> None:
    message.save()
