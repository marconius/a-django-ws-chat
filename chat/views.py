from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import Http404, HttpResponseForbidden
from django.shortcuts import render

from .exceptions import InvalidChatRoomName
from .models import ChatRoom


@login_required(login_url='/admin/login/')
def index(request):
    authenticated_user_name = request.user.username
    users = User.objects.filter(is_active=True).exclude(username=authenticated_user_name)

    available_user_chat_rooms = [
        (
            '{} (YOU!)'.format(authenticated_user_name),
            ChatRoom(frozenset([authenticated_user_name]))
        )
    ]
    for user in users:
        room_info = (
            user.username,
            ChatRoom(frozenset([authenticated_user_name, user.username]))
        )
        available_user_chat_rooms.append(room_info)

    return render(
        request,
        'chat/index.html',
        {'available_user_chat_rooms': available_user_chat_rooms})

@login_required(login_url='/admin/login/')
def room(request, room_name):
    try:
        chat_room = ChatRoom.from_room_name(room_name)
    except InvalidChatRoomName:
        raise Http404('Chat room does not exist')

    if not chat_room.check_is_member(request.user.username):
        return HttpResponseForbidden('This is a private chat room')

    return render(request, 'chat/room.html', {
        'room_name': room_name
    })
