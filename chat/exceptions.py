"""Exceptions that the chat app may encounter"""

class InvalidChatRoomName(Exception):
    """A chat room with this name is impossible"""
    pass
