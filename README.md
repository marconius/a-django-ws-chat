# A Chat App

Thanks for taking the time to look at my django websocket-based chat server.

My goals with this exercise were to demonstrate my fundamental knowledge of Python and my enthusiasm to quickly get up
to speed with modern Python. This basic approach was at the heart of the choices detailed in the Choices section.


## Requirements

* Python 3.7.6 or 3.8.1

_Other versions of 3.7 will likely work, but project was only tested with 3.7.6._


## Let's Try Out!


### 1. Start the server

From project root

```
$ pip install -r requirements.txt
$ ./manage.py migrate
$ ./manage.py runserver
```


### 2. Login with User 1

Visit http://localhost:8000/chat where you can log in with password `Testing123` using one of the users found in data
migration  `chat/migrations/0003_auto_20200129_1836.py`.

e.g: marco:Testing123

Click to chat to **kindra** to open the private chat room

_Note that you can create new users, but they must be set as admins to be able to log in, because this project
 piggy-backs the admin site for its login page_


### 3. Log in with User 2

Open an incognito or private window and visit http://localhost:8000/chat. Log in as a "kindra", and join the chat with
first user.

i.e: kindra:Testing123


### 4. Chat

Send messages back and forth.


### 5 Re-Entering Chat Rooms

The chat history will be reloaded every time you re-enter a private chat.

However, due to the simplicity of the chat client, a forced re-rendering of the page's elements is necessary. In other
words, clicking on the chat room's link or re-entering its address in the browser's address bar will work as expected,
but using "back" and "forward" browser buttons may repeat archived messages.


## Choices

"Explain the choices you made and why you made them."


### Why No Tests?

Seeing as how tests were present in the other projects I presented, I figured it best to save a bit of time by not
writing tests.


### The Stack

The options I evaluated for the different technical aspects


#### Protocol

* HTTP Long Polling: Familiar protocol and codes, but is still polling, albeit not repeatedly
* ☑ WebSocket: Easy browser side api, only one connection per client required, somewhat familiar from previous work.


#### Server

* DIY: implementing something directly in an app using a library like
  [`websocket`](https://pypi.python.org/pypi/websocket) would have been interesting, but too risky considering the
  timeframe
* [django-websocket-redis](https://django-websocket-redis.readthedocs.io/en/latest/): has the advantage of being
  opinionionated, and reducing the amount of decisions I would have to make. Runs with `uwsgi` making it more portable
  in theory if deploying
* ☑ [Django Channels](https://channels.readthedocs.io/en/latest/): Best docs, clean and clear architecture, employes
  `async`/`await` syntax, can run during development without Redis.


#### Front-end

* Front-end framework: safer and cleaner re-rendering and handling of user initiated events.
* ☑ no front-end framework: fast. This was my ultimate choice, as my goal here was to demonstrate _Python_ code.


### Why start from tutorial code?

The basis of this project is [Channel's Tutorial](https://channels.readthedocs.io/en/latest/tutorial/index.html).
I assumed that it is fine to start from this boiler plate code, because I judged that
A. there was significant code to be written to achieve the requirements stipulated by the assignement description even
with the bolier plate code and
B. the initial code was so simple that it would have been difficult to rewrite it very differently after reading the
tutorial.

Do `git diff <initial commit hash>` to see exactly the code I was responsible for.


## Scalability

Following are a few notes regarding the directive to "Build this message center preparing for it to be used at scale."


### Scaling Horizontally

The application itself, as far as I can tell, will handle scaling (horizontally) thanks to Channel's concept of Layers,
which ensures that multiple instances of the application will be able to communicate.

Having said that, the application will need to be configured for production in order to enable this advantage. Namely,
using a remote relational database and using something like `channels-redis` to send layer-level communication via a
remote Redis instance or cluster.

### Drawbacks

The principle bottleneck that I can identify in my chat app is sending the entire chat history via a websocket frame

1. The number of messages will get pretty unmanageable after the history grows
2. Fetching the archived messages is "awaited" in the consumer so that we can ensure that it is sent to the chat client
   _before_ anything else, which goes against much of the advantages of using coroutines.

Both these drawbacks were intended to make it a lot easier to implement the client, as all it needs to know is how to
display batches of messages in the order that they are received
